var gulp           = require('gulp'),
    livereload     = require('gulp-livereload'),
    browserSync    = require('browser-sync'),
    mainBowerFiles = require('gulp-main-bower-files'),
    filter         = require('gulp-filter'),
    concat         = require('gulp-concat'),
    less           = require('gulp-less'),
    babel          = require('gulp-babel'),
    sass           = require('gulp-sass');

var config = {
    jsPath: 'js/**/*.js',
    sassPath: 'sass/main.scss',
    buildPath: 'build'
};


gulp.task('default', ['browser-sync', 'bower', 'javascript', 'sass', 'watch'], function() {});


gulp.task('watch', function() {
    gulp.watch([config.jsPath], ['javascript']);
    gulp.watch([config.sassPath], ['sass']);
    gulp.watch(
        [
            '../app/presenters/**/*.php',
            '../app/presenters/templates/**/*.latte',
            'build/*.css',
            'build/*.js'
        ]).on('change', browserSync.reload);
});


gulp.task('bower', function() {

    var jsFilter = filter(['**/*.js'], {restore: true});
    var lessFilter = filter(['**/*.less'], {restore: true});

    gulp.src('bower.json')

        // save javascript files
        .pipe(mainBowerFiles(), { base: 'bower_components/'})
        .pipe(jsFilter)
        .pipe(concat('bower.js'))
        .pipe(gulp.dest(config.buildPath))
        .pipe(jsFilter.restore)

        // less files from bootstrap
        .pipe(lessFilter)
        .pipe(less())
        .pipe(concat('bower.css'))
        .pipe(gulp.dest(config.buildPath))

});


gulp.task('javascript', function() {
    return gulp.src(config.jsPath)
        .pipe(babel()) // new ecmascript support
        .pipe(concat('main.js'))
        .pipe(gulp.dest('build'));
});

gulp.task('sass', function() {
    return gulp.src(config.sassPath)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build'));
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "http://localhost/tests/www/",
        open: true
    });
});


